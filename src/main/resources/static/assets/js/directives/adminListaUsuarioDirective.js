angular.module('app.directives')
  .directive("usuariosLista", usuariosLista);

  usuariosLista.$inject = ['$rootScope'];

  function usuariosLista($rootScope){
      $rootScope.title = 'Lista de Usuários'; //<title> </title>

      return {
        restrict: 'E',
  			templateUrl: 'assets/js/view/admin_listaUsuario.html'
      }
  }
