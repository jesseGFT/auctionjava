angular.module('app.directives')
  .directive("userHome", userHome);

  userHome.$inject = ['$rootScope'];

  function userHome($rootScope){
      $rootScope.title = 'Home'; //<title> </>

      return {
        restrict: 'E',
  			templateUrl: 'assets/js/view/user_home.html'
      }
  }
