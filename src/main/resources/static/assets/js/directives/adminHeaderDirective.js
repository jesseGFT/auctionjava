angular.module('app.directives')
  .directive("headerSidebarAdmin", headerSidebarAdmin);

  function headerSidebarAdmin(){
      return {
        restrict: 'E',
  			templateUrl: 'assets/js/view/admin_header.html'
      }
  }
