angular.module('app.directives')
  .directive("produtosLista", produtosLista);

  produtosLista.$inject = ['$rootScope'];

  function produtosLista($rootScope){
      $rootScope.title = 'Lista de Produtos'; //<title> </>

      return {
        restrict: 'E',
  			templateUrl: 'assets/js/view/admin_listaProduto.html'
      }
  }
