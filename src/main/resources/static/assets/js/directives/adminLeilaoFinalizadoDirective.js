angular.module('app.directives')
  .directive("leilaoFinalizado", leilaoFinalizado);

  leilaoFinalizado.$inject = ['$rootScope'];

  function leilaoFinalizado($rootScope){
      $rootScope.title = 'Iniciar Leilão'; //<title> </>

      return {
        restrict: 'E',
  			templateUrl: 'assets/js/view/admin_leilaoFinalizado.html'
      }
  }
