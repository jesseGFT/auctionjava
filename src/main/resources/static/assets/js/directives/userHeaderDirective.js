angular.module('app.directives')
  .directive("headerSidebarUser", headerSidebarUser);

  function headerSidebarUser(){
      return {
        restrict: 'E',
  			templateUrl: 'assets/js/view/user_header.html'
      }
  }
