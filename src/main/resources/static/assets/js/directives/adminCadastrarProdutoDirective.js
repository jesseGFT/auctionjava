angular.module('app.directives')
  .directive("cadastrarProduto", cadastrarProduto);

  cadastrarProduto.$inject = ['$rootScope'];

  function cadastrarProduto($rootScope){

      return {
        restrict: 'E',
  			templateUrl: 'assets/js/view/admin_cadastrarProduto.html'
      }
  }
