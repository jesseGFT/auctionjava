angular.module('app.directives')
  .directive("loginCadastrarUsuario", loginCadastrarUsuario);

  loginCadastrarUsuario.$inject = ['$rootScope'];

  function loginCadastrarUsuario($rootScope){

      return {
        restrict: 'E',
  			templateUrl: 'assets/js/view/login_cadastrarUsuario.html'
      }
  }
