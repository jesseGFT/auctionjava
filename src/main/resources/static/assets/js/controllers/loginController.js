angular.module('app.controllers')
  .controller("loginController", loginController);

    loginController.$inject = ['$http', 'accessFactory', '$rootScope'];

    function loginController($http, accessFactory, $rootScope){
      const vm = this;// vm = view <-> model (conexão entre as partes)

      $rootScope.title = 'Login'; //<title> </>

      vm.showMessageError = false;
      vm.validar = validar;
      vm.cadastrarUsuario = cadastrarUsuario;
      
      vm.login = {
       email: "adm@gft.com", //pode ser o netid ou o email
       senha: "adm"
      }

      function validar(login){// get com parâmetro: header, params (objeto),
        //function validar(naoprecisaserigualanada)
        const req = {
            url: '/login-rest',
            method: 'GET',
            headers: {'Content-Type': 'application/json;charset=utf-8'},
            params: {"email": login.email, "senha": login.senha}
            //params: login //assim passa um objeto completo
          }

          // console.log(req);

          $http(req)
            .then(
              function(res){
                if(accessFactory.checkAccess(res.data)){ //se quiser o status = res.status =P
                    vm.showMessageError = false;
                }else {
                    vm.showMessageError = true;
                }
                accessFactory.checkAccess(res.data);
              },

              function(res){

              }
            )
      }

      function cadastrarUsuario(usuario){
        console.log("Entrou no cadastro de user");
        const req = {
          url: '/registrarUsuarioRest',
          method: 'POST',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
          params: {"nome": usuario.nome, "sobrenome": usuario.sobrenome, "email": usuario.email, "netId": usuario.netId, "cpf": usuario.cpf, "unidade": usuario.unidade, "senha": usuario.senha}
        }

        console.log(req);
        $http(req)
          .then(
            function(res){
              //changeVisibilityCadastrar();
              if(accessFactory.checkStatus(res.status)){
                vm.showMessageError = false;
                vm.dialog.error = true;
              }else {
                vm.showMessageError = true;
              }
                accessFactory.checkStatus(res.status);
            },

            function(res){

            }
          )
        }

    }
