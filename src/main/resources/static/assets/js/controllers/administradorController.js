angular.module('app.controllers')
  .controller("administradorController", administradorController);

  administradorController.$inject = ['$http', 'accessFactory', '$httpParamSerializerJQLike'];

  function administradorController ($http, accessFactory, $httpParamSerializerJQLike){
    const vm = this;

    vm.showMessageError = false;

    vm.cadastrarUsuario = cadastrarUsuario; //Cadastra usuário
    vm.editarUsuario = editarUsuario;
    vm.atualizarUsuario = atualizarUsuario; //Atualiza usuário
    vm.desativarUsuario = desativarUsuario; //Desativa usuário

    vm.redefinirSenha = redefinirSenha;
    vm.listarTodosUsuarios = listarTodosUsuarios(); //para deixar autoexecutável é só colocar o nome com parenteses no final
    vm.usuarios = [];

    vm.cadastrarProduto = cadastrarProduto; //Cadastra produto
    vm.editarProduto = editarProduto; //esta função cria uma cópia dos elementos para o formulário de editar produto
    vm.atualizarProduto = atualizarProduto; //Atualiza produto
    vm.desativarProduto = desativarProduto; //Desativa produto
    vm.listarTodosProdutos = listarTodosProdutos();
    vm.produtos = [];

    // vm.diolog = {
    //   cadastrar: false,
    //   editar: false,
    //   desativar: false
    // }

    // vm.dialog.cadastrar = true;
    // vm.dialog.cadastrar = false;
    // vm.changeVisibilityCadastrar = changeVisibilityCadastrar;
    //
    // function changeVisibilityCadastrar(){
    //   vm.dialog.cadastrar = !vm.dialog.cadastrar;
    // }

    function cadastrarUsuario(usuario){
      console.log("Entrou no cadastro de user");
      const req = {
        url: '/registrarUsuarioRest',
        method: 'POST',
        headers: {'Content-Type': 'application/json;charset=utf-8'},
        params: {"nome": usuario.nome, "sobrenome": usuario.sobrenome, "email": usuario.email, "netId": usuario.netId, "cpf": usuario.cpf, "unidade": usuario.unidade, "senha": usuario.senha}
      }

      console.log(req);
      $http(req)
        .then(
          function(res){
            if(accessFactory.checkStatus(res.status)){
              vm.showMessageError = false;
              vm.dialog.error = true;
            }else {
              vm.showMessageError = true;
            }
              accessFactory.checkStatus(res.status);

              listarTodosUsuarios();
          },

          function(res){

          }
        )
      }

      function cadastrarProduto(produto){
        console.log("Entrou no cadastro de produto");
        const req = {
          url: '/registrarProdutoRest',
          method: 'POST',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
          params: {"nomeProduto": produto.nomeProduto, "descricao": produto.descricao, "codPatrimonio": produto.codPatrimonio, "unidade": produto.unidade, "valorInicial": produto.valorInicial, "valorPorLance": produto.valorPorLance}
        }

        console.log(req);
        $http(req)
          .then(
            function(res){
              if(accessFactory.checkStatus(res.status)){
                vm.showMessageError = false;
                vm.dialog.error = true;
              }else {
                vm.showMessageError = true;
              }
                accessFactory.checkStatus(res.status);

                listarTodosProdutos();
            },

            function(res){

            }
          )
        }

        function editarUsuario(usuario, index){
          vm.form = angular.copy(usuario);
          vm.form.index = usuario.id_usuario;
          console.log("Loga ao clicar no editar usuário, traz o id: " + vm.form.index);
          console.log("Administrador.form.id_usuario: " + Administrador.form.id_usuario);
        }

        function editarProduto(produto){
          vm.form = angular.copy(produto);
          vm.form.index = produto.id_produto;
          console.log(vm.form.index);
        }

        function atualizarUsuario(usuario){

          const req = {
              url: '/updateUsuario',
              method: 'POST',
              headers: {'Content-Type': 'application/json;charset=utf-8'},
              params: {"id_usuario": usuario.id_usuario, "nome": usuario.nome, "sobrenome": usuario.sobrenome, "email": usuario.email, "netId": usuario.netId, "cpf": usuario.cpf, "unidade": usuario.unidade, "senha": usuario.senha}
            }

            $http(req)
              .then(
                function(res){
                  if(accessFactory.checkStatus(res.status)){
                      vm.showMessageError = false;
                  }else {
                      vm.showMessageError = true;
                  }
                  accessFactory.checkStatus(res.status);
                  listarTodosUsuarios();
                },
                function(res){

                }

              )
        }

        function atualizarProduto(produto){

          const req = {
              url: '/updateProduto',
              method: 'POST',
              headers: {'Content-Type': 'application/json;charset=utf-8'},
              params: {"idProduto": produto.idProduto, "nomeProduto": produto.nomeProduto, "descricao": produto.descricao, "codPatrimonio": produto.codPatrimonio, "unidade": produto.unidade, "valorInicial": produto.valorInicial, "valorPorLance": produto.valorPorLance}
            }

            $http(req)
              .then(
                function(res){
                  if(accessFactory.checkStatus(res.status)){
                      vm.showMessageError = false;
                  }else {
                      vm.showMessageError = true;
                  }
                  accessFactory.checkStatus(res.status);
                  listarTodosUsuarios();
                },
                function(res){

                }

              )
         }

    function desativarUsuario(usuario){
      const req = {
          url: '/updateDestivarUsuario',
          method: 'POST',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
          params: {"id_usuario": usuario.id_usuario}
        }

        $http(req)
          .then(
            function(res){
                if(accessFactory.checkStatus(res.status)){
                    vm.showMessageError = false;
                }else {
                    vm.showMessageError = true;
                }
                accessFactory.checkStatus(res.status);
            },
            function(res){

            }
        )
    }

    function redefinirSenha(usuario, index){
      vm.form = angular.copy(usuario);
      vm.form.index = usuario.id_usuario;
      console.log("PEGA O ID:" + vm.form.index);

      const req = {
        url: '/updateSenha',
        method: 'PUT',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        //headers: {'Content-Type': 'application/json;charset=utf-8'},
        data: {"id_usuario": usuario.id_usuario}
        //data: id_usuario=+usuario.id_usuario
      }

      $http(req)
        .then(
          function(res){
              if(accessFactory.checkStatus(res.status)){
                vm.showMessageError = false;
              }else {
                vm.showMessageError = true;
              }
                accessFactory.checkStatus(res.status);
              },
              function(res){

              }
            )
      }

      function listarTodosUsuarios(){
        const req = {
          url: '/listaTodosUser',
          method: 'POST',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
        }

        $http(req)
          .then(
            function(res){

              vm.usuarios = res.data;
              console.log("TODOS USUARIOS: " + vm.usuarios);

            },
            function(res){

            }
          )
        }

        function listarTodosProdutos(){
          const req = {
            url: '/listaTodosProdutos',
            method: 'POST',
            headers: {'Content-Type': 'application/json;charset=utf-8'},
          }

          $http(req)
            .then(
              function(res){

                vm.produtos = res.data;
                console.log("TODOS PRODUTOS: " + vm.produtos);

                },
                function(res){

                }
              )
          }
  }
