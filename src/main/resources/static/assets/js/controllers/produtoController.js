angular.module('app.controllers')
  .controller("produtoController", produtoController);

  produtoController.$inject = ['$http', 'accessFactory'];

  function produtoController($http, accessFactory){
    const vm = this;

    vm.showMessageError = false;
    vm.cadastrar = cadastrar; //Cadastra produto
    // vm.atualizar = atualizar; //Atualiza produto
    // vm.desativar = desativar; //Desativa produto
    // vm.listarTodos = listarTodos(); //para deixar autoexecutável deixar o nome com parenteses no final
    // vm.produtos = [];

    function cadastrar(produto){
      console.log("Entrou no cadastro de produto");
      const req = {
        url: '/registrarProdutoRest',
        method: 'POST',
        headers: {'Content-Type': 'application/json;charset=utf-8'},
        params: {"nome": produto.nome, "descricao": produto.descricao, "email": produto.email, "netId": produto.netId, "cpf": produto.cpf, "unidade": produto.unidade, "senha": produto.senha}
      }

      console.log(req);
      $http(req)
        .then(
          function(res){
            if(accessFactory.checkStatus(res.status)){
              vm.showMessageError = false;
            }else {
              vm.showMessageError = true;
            }
              accessFactory.checkStatus(res.status);
          },

          function(res){

          }
        )
      }

          // function atualizar(produto){
          //   const req = {
          //       url: '/updateProduto',
          //       method: 'PUT',
          //       headers: {'Content-Type': 'application/json;charset=utf-8'},
          //       params: {nome: produto.nome, sobrenome: produto.sobrenome, email: produto.email, netid: produto.netId, cpf: produto.cpf, unidade: produto.unidade, senha: produto.senha}
          //     }
          //
          //     $http(req)
          //       .then(
          //         function(res){
          //             if(updaterFactory.atualizarUsuario(res.data)){
          //                 vm.showMessageError = false;
          //             }else {
          //                 vm.showMessageError = true;
          //             }
          //             updaterFactory.atualizarUsuario(res.data);
          //         },
          //         function(res){
          //
          //         }
          //       )
          // }
          //
          // function desativar(produto){
          //   const req = {
          //       url: '/updateDestivarProduto',
          //       method: 'GET',
          //       headers: {'Content-Type': 'application/json;charset=utf-8'},
          //       params: {id_usuario: produto.id_usuario, nome: produto.nome, sobrenome: produto.sobrenome, email: produto.email, netid: produto.netId, cpf: produto.cpf, unidade: produto.unidade, senha: produto.senha}
          //     }
          //
          //     $http(req)
          //       .then(
          //         function(res){
          //             if(deleterFactory.desativarUsuario(res.data)){
          //                 vm.showMessageError = false;
          //             }else {
          //                 vm.showMessageError = true;
          //             }
          //             deleterFactory.desativarUsuario(res.data);
          //         },
          //         function(res){
          //
          //         }
          //       )
          // }
          //

  }
