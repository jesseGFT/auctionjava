angular.module('app.controllers')
  .controller("leilaoController", leilaoController); //defini nome da função

 leilaoController.$inject = ['$http', '$rootScope'];

  function leilaoController($http, $rootScope){
    
    const vm = this;

    vm.leiloes = [];
    vm.getLeiloes = getLeiloes(); //qualquer coisa com vm é carregado automaticamente

    $rootScope.title = 'Home Yabadaba';

    function getLeiloes(){// get com parâmetro: header, params (objeto),
      const req = {
          url: 'assets/data/leilao.json',
          //url: '10.230.8.38:8083/login',
          method: 'GET',
          headers: {'Content-Type': 'application/json;charset=utf-8'}
        }
        console.log(req);
        $http(req)
          .then(
            function(res){
                vm.leiloes = res.data; //retorno + corpo do retorno ()json completo
                console.log("Deu certo");
            },
            function(res){
                //erro
                console.log("Deu erro");
            }
          )
    }

  }
