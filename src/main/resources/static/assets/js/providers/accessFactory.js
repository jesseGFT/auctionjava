angular.module('app.providers')
	.factory("accessFactory", accessFactory);

accessFactory.$inject = ['$rootScope', '$location'];

function accessFactory($rootScope, $location) {

		function checkAccess(user) {
			console.log("checkAccess", user);
			if(user.tipo.id == 1){
				$rootScope.user = user;
				$location.path('/administrador'); //ROTAS
				return true
			}else if (user.tipo.id == 2) {
				$rootScope.user = user;
				$location.path('/usuario'); //ROTAS
				return true
			}else {
				console.log("Esta msg aparece quando o tipo do usuário não é 1 nem 2");
				return false;
			}
		}

		function checkStatus(status) {
			console.log("Entrou na função 'checkStatus'. O status é: ", status);
		  switch (status) {
		  	case '200':
		    	console.log("Usuário Cadastrado com Sucesso");
		      break;
		    case '300':
		      alert("Erro 300");
		      break;
				case '404':
				  alert("Erro 404");
				  break;
				case '500':
					alert("Erro 500");
					break;
		    default:
		  }
		}

		return {
			checkAccess: checkAccess,
			checkStatus: checkStatus
		}

};
