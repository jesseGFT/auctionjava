angular.module('app', [
  'app.controllers',
  'app.directives',
  'app.providers',
  'app.routes'
]);
