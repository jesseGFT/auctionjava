angular.module('app.routes', ['ngRoute'])
    .config(config);

    config.$inject = ['$routeProvider', '$locationProvider'];

    function config($routeProvider, $locationProvider){

      $routeProvider
          .when('/', {
            templateUrl: 'assets/js/view/login.html',
            controller: 'loginController',
            controllerAs: 'Login'
          })
          .when('/administrador', {
            templateUrl: 'assets/js/view/admin_main.html',
            controller: 'administradorController',
            controllerAs: 'Administrador'
          })
          .when('/usuario', {
            templateUrl: 'assets/js/view/user_main.html',
            controller: 'usuarioController',
            controllerAs: 'Usuario'
          })
          .otherwise ({ redirectTo: '/' });

          $locationProvider.html5Mode(true);
    };
