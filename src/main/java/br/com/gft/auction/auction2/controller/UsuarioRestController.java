package br.com.gft.auction.auction2.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.gft.auction.auction2.model.Usuario;
import br.com.gft.auction.auction2.service.UsuarioService;

@Controller
public class UsuarioRestController {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private void setService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	// public ResponseEntity<String> teste(@RequestParam("login") Login param) {
	// return null;
	// }

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login() {
		return "index";
	}

	@RequestMapping(value = "/login-rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Usuario> TipoJson(@RequestParam("email") String email, @RequestParam("senha") String senha) {

		Usuario user = usuarioService.findUserByEmailAndSenha(email, senha);		
		if (user != null) {
			if (user.getTipo().getRole().equals("ADMIN")) {
				return new ResponseEntity<Usuario>(user, HttpStatus.OK);
			} else {
				return new ResponseEntity<Usuario>(user, HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<Usuario>(HttpStatus.BAD_REQUEST);
		}

	}

	// Authentication auth =
	// SecurityContextHolder.getContext().getAuthentication();
	//
	// if (auth != null && auth.isAuthenticated() &&
	// !auth.getPrincipal().equals("anonymousUser")) {
	// Usuario user = usuarioService.findUserByEmail(auth.getName());
	// if (user != null) {
	// if (user.getTipo().getRole().equals("ADMIN")) {
	// return new ResponseEntity<String>("Você é um Admin ", HttpStatus.OK);
	// } else {
	// return new ResponseEntity<String>("Você é um Usuario ",
	// HttpStatus.OK);
	// }
	// } else {
	// return new ResponseEntity<String>("Usuário não encontrado",
	// HttpStatus.BAD_REQUEST);
	// }
	// } else {
	// return new ResponseEntity<String>("Senha ou usuario Invalida",
	// HttpStatus.BAD_REQUEST);
	// }

	@RequestMapping(value = "/registrarUsuarioRest", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Usuario> createNewUser(@Valid Usuario user, BindingResult bindingResult) {
		// Verifica se existe usuario com este email
		Usuario userExists = usuarioService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			return new ResponseEntity<Usuario>(user, HttpStatus.BAD_REQUEST);
		}
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<Usuario>(user, HttpStatus.BAD_REQUEST);
		} else {
			usuarioService.saveUser(user);
			return new ResponseEntity<Usuario>(user, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/listaTodosUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseEntity<?> getUserJson() {
		List<Usuario> user = usuarioService.findByActive();
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarUmUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<?> getPersonById(@RequestParam("id_usuario") long idUsuario) {
		Usuario user = usuarioService.findUserById(idUsuario);
		if (user == null) {
			return new ResponseEntity<>("usuario não encontrado", HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(user, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateDestivarUsuario", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<?> DesativaUsuario(@RequestParam("id_usuario") long idUsuario) {
		Usuario user = usuarioService.findUserById(idUsuario);
		if (user == null) {
			return new ResponseEntity<>("Não existe usuario", HttpStatus.BAD_REQUEST);
		}
		if (usuarioService.findUserById(idUsuario) == null) {
			return new ResponseEntity<>("ID do usuario não encontrado", HttpStatus.NOT_FOUND);
		}

		long rowsUpdated = usuarioService.updateSituacao(idUsuario);

		if (rowsUpdated == 1) {
			return new ResponseEntity<>("Usuario alterado", HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>("Usuario maise de um id encotrado", HttpStatus.CONFLICT);
		}
	}

	@RequestMapping(value = "/updateSenha", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<?> updateSenhaUser(@RequestParam("id_usuario") long idUsuario) {
		Usuario user = usuarioService.findUserById(idUsuario);
		if (user == null) {
			return new ResponseEntity<>("Não existe usuario", HttpStatus.BAD_REQUEST);
		}
		if (usuarioService.findUserById(idUsuario) == null) {
			return new ResponseEntity<>("ID do usuaraio não encontrado", HttpStatus.NOT_FOUND);
		}

		long rowsUpdated = usuarioService.updateSenhaUser(idUsuario);

		if (rowsUpdated == 1) {
			return new ResponseEntity<>("Usuario alterado", HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>("Usuario maise de um id encotrado", HttpStatus.CONFLICT);
		}
	}

	@RequestMapping(value = "/updateUsuario", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<?> updateD(@RequestParam("id_usuario") long id_usuario, @RequestParam("nome") String nome,
			@RequestParam("sobrenome") String sobrenome, @RequestParam("email") String email,
			@RequestParam("netId") String netId, @RequestParam("cpf") long cpf,
			@RequestParam("unidade") String unidade) {

		Usuario user = usuarioService.findUserById(id_usuario);
		if (user == null) {
			return new ResponseEntity<>("usuario não existe usuario", HttpStatus.BAD_REQUEST);
		}
		if (usuarioService.findUserById(id_usuario) == null) {
			return new ResponseEntity<>("ID do usuario não encontrado", HttpStatus.NOT_FOUND);

		} else {
			usuarioService.updateUsuario(id_usuario, nome, sobrenome, email, netId, cpf, unidade);
			return new ResponseEntity<>("Update com sucesso", HttpStatus.NO_CONTENT);
		}
	}

}
