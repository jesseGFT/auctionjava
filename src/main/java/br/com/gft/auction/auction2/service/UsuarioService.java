package br.com.gft.auction.auction2.service;

import java.util.List;

import br.com.gft.auction.auction2.model.Usuario;

public interface UsuarioService {

	public Usuario findUserByEmail(String email);

	public Usuario findUserByEmailAndSenha(String email,String senha);
	
	public List<Usuario> findByActive();

	public void saveUser(Usuario user);

	public Usuario findUserById(long id_usuario);

	public int updateSituacao(long id_usuario);
	
	public int updateSenhaUser(long id_usuario);
	
	public int updateUsuario(long id_usuario, String nome,String sobrenome,String email,String netId
			,long cpf, String unidade);
	
}
