package br.com.gft.auction.auction2.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.gft.auction.auction2.model.Usuario;


public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	Usuario findByEmail(String email);
	
	@Query("select u from usuario u where u.email= :email and u.senha= :senha")
	Usuario findByEmailAndSenha(@Param ("email")String email,@Param ("senha") String senha);
	// Usuario findByTipo(String tipo);
	
	@Query("select u from usuario u where u.situacao = 1")
	public List<Usuario> findAll();	
	
	
	@Modifying
	@Transactional 
	@Query("UPDATE usuario u SET u.situacao =0 WHERE u.id_usuario = :id_usuario")
	int updateSituacao(@Param("id_usuario") long id_usuario);
	
	@Modifying
	@Transactional 
	@Query("UPDATE usuario u SET u.senha = 'gft123' WHERE u.id_usuario = :id_usuario")
	int updateSenha(@Param("id_usuario") long id_usuario);		
	
	@Modifying
	@Transactional 
	@Query("UPDATE usuario u SET u.nome= :nome,u.sobrenome= :sobrenome,u.email= :email,u.netId= :netId,u.cpf= :cpf,u.unidade= :unidade WHERE u.id_usuario = :id_usuario")
	int updateUsuario(@Param("id_usuario") long id_usuario,@Param("nome") String nome,@Param("sobrenome") String sobrenome,@Param("email") String email,@Param("netId") String netId
			,@Param("cpf") long cpf,@Param("unidade") String unidade);
}
