package br.com.gft.auction.auction2.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.gft.auction.auction2.model.Usuario;
import br.com.gft.auction.auction2.model.Usuario.SituacaoEnum;
import br.com.gft.auction.auction2.repository.UsuarioRepository;

@Service("usuarioService")
public class UsuarioServiceImpl implements UsuarioService {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public Usuario findUserByEmail(String email) {
		return usuarioRepository.findByEmail(email);
	}

	@Override
	public void saveUser(Usuario user) {
//		user.setSenha(bCryptPasswordEncoder.encode(user.getSenha()));
		user.setSituacao(SituacaoEnum.ATIVO.getSituacaoEnum());
		usuarioRepository.save(user);
	}

	@Override
	public Usuario findUserById(long id_usuario) {
		return usuarioRepository.findOne(id_usuario);
	}

	@Override
	public int updateSituacao(long id_usuario) {
		return usuarioRepository.updateSituacao(id_usuario);
	}

	@Override
	public int updateSenhaUser(long id_usuario) {
		// user.setSenha(bCryptPasswordEncoder.encode(user.getSenha()));
		return usuarioRepository.updateSenha(id_usuario);
	}

	@Override
	public int updateUsuario(long id_usuario, String nome, String sobrenome, String email, String netId, long cpf,
			String unidade) {
		return usuarioRepository.updateUsuario(id_usuario, nome, sobrenome, email, netId, cpf, unidade);
	}

	@Override
	public List<Usuario> findByActive() {		
		return usuarioRepository.findAll();
	}

	@Override
	public Usuario findUserByEmailAndSenha(String email, String senha) {		
		return usuarioRepository.findByEmailAndSenha(email, senha);
	}

}
