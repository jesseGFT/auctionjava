package br.com.gft.auction.auction2.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.gft.auction.auction2.model.Produto;
import br.com.gft.auction.auction2.service.ProdutoService;

@Controller
public class ProdutoRestController {

	@Autowired
	private ProdutoService produtoService;

	@Autowired
	private void setService(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}

	// metodo responsavel por criar um novo produto
	@RequestMapping(value = "/registrarProdutoRest", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> createNewUser(@Valid Produto produto, BindingResult bindingResult) {
		// Verifica se existe Produto este codigo de Patrimonio
		Produto produtoExists = produtoService.findProdutoBycodPatrimonio(produto.getCodPatrimonio());
		if (produtoExists != null) {
			return new ResponseEntity<String>("Produto já existe", HttpStatus.BAD_REQUEST);
		}
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<String>("verifique o forulario", HttpStatus.BAD_REQUEST);
		} else {
			produtoService.saveProduto(produto);
			return new ResponseEntity<String>("insert com sucesso", HttpStatus.OK);
		}
	}

	// metodo responsavel por listar todos os Produtos
	@RequestMapping(value = "/listaTodosProdutos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseEntity<?> getProdutoJson() {
		List<Produto> produto = produtoService.findByActive();
		return new ResponseEntity<>(produto, HttpStatus.OK);
	}

	// metodo responsavel por selecionar um Produto
	@RequestMapping(value = "/listarUmProduto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<?> getProdutoById(@RequestParam("idProduto") long idProduto) {
		Produto produto = produtoService.findProdutoById(idProduto);
		if (produto == null) {
			return new ResponseEntity<>("Produto não existe", HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(produto, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateDestivarProduto", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<?> DesativaUsuario(@RequestParam("idProduto") long idProduto) {
		Produto produto = produtoService.findProdutoById(idProduto);
		if (produto == null) {
			return new ResponseEntity<>("Produto Não existe", HttpStatus.BAD_REQUEST);
		}
		if (produtoService.findProdutoById(idProduto) == null) {
			return new ResponseEntity<>("ID do Produto não encontrado", HttpStatus.NOT_FOUND);
		}

		long rowsUpdated = produtoService.updateSituacao(idProduto);

		if (rowsUpdated == 1) {
			return new ResponseEntity<>("Produto Desativado", HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>("Mais de um id Produto encotrado", HttpStatus.CONFLICT);
		}
	}

	@RequestMapping(value = "/updateProduto", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ResponseEntity<?> updateD(@RequestParam("idProduto") long idProduto,
			@RequestParam("descricao") String descricao, @RequestParam("nomeProduto") String nomeProduto,
			@RequestParam("codPatrimonio") String codPatrimonio, @RequestParam("unidade") String unidade,
			@RequestParam("valorInicial") double valorInicial, @RequestParam("valorPorLance") double valorPorLance) {
		
		Produto produto = produtoService.findProdutoById(idProduto);
		if (produto == null) {
			return new ResponseEntity<>("Produto não existe", HttpStatus.BAD_REQUEST);
		}
		if (produtoService.findProdutoById(idProduto) == null) {
			return new ResponseEntity<>("ID do produto não encontrado", HttpStatus.NOT_FOUND);

		} else {
			produtoService.updateProduto(idProduto, descricao, nomeProduto, codPatrimonio, unidade,	valorInicial, valorPorLance);
			return new ResponseEntity<>(produto, HttpStatus.NO_CONTENT);			
		}
	}
}
