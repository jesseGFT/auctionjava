package br.com.gft.auction.auction2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@SpringBootApplication
public class Auction2Application {

	public static void main(String[] args) {
		SpringApplication.run(Auction2Application.class, args);
	}
	
	@Bean
	public javax.sql.DataSource dataSource(){				
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		dataSource.setUsername("HR");
		dataSource.setPassword("HR");
		dataSource.setUrl("jdbc:oracle:thin:@localhost:1521:XE");
		return dataSource;
		
	}
}
