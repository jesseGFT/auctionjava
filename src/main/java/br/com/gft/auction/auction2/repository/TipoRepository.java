package br.com.gft.auction.auction2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gft.auction.auction2.model.TipoUsuario;


public interface TipoRepository extends JpaRepository<TipoUsuario, Integer> {
	TipoUsuario findByTipo(TipoUsuario tipo);

}
