package br.com.gft.auction.auction2.service;

import java.util.List;

import br.com.gft.auction.auction2.model.Produto;

public interface ProdutoService {
	
	public void saveProduto(Produto produto);
	
	public List<Produto> findByActive();
	
	public Produto findProdutoById(long idProduto);
	
	public Produto findProdutoBycodPatrimonio(String codPatrimonio);
		
	public int updateSituacao(long idProduto);
	
	public int updateProduto(long idProduto,String descricao, String nomeProduto,String codPatrimonio,String unidade,double valorInicial, double valorPorLance);
}
