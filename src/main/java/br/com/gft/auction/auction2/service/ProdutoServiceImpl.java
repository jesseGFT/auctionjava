package br.com.gft.auction.auction2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gft.auction.auction2.model.Produto;
import br.com.gft.auction.auction2.model.Usuario.SituacaoEnum;
import br.com.gft.auction.auction2.repository.ProdutoRepository;

@Service("produtoService")
public class ProdutoServiceImpl implements ProdutoService{

	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Override
	public void saveProduto(Produto produto) {
		produto.setSituacao(SituacaoEnum.ATIVO.getSituacaoEnum());
		produtoRepository.save(produto);
				
	}

	@Override
	public Produto findProdutoById(long idProduto) {
		return produtoRepository.findOne(idProduto);
	}

	@Override
	public Produto findProdutoBycodPatrimonio(String codPatrimonio) {
		return produtoRepository.findByCodPatrimonio(codPatrimonio);
	}


	@Override
	public int updateSituacao(long idProduto) {		
		return produtoRepository.updateSituacao(idProduto);
	}

	@Override
	public int updateProduto(long idProduto,String descricao, String nomeProduto,String codPatrimonio,String unidade,double valorInicial, double valorPorLance) {		
		return produtoRepository.updateProduto(idProduto, descricao, nomeProduto, codPatrimonio, unidade, valorInicial, valorPorLance);
	}

	@Override
	public List<Produto> findByActive() {		
		return produtoRepository.findAll();
	}


}
