package br.com.gft.auction.auction2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "unidade")
public class Unidade {
	
	@Id	
	private long unidade_id;
	
	@Column(name = "unidade")
	private String unidade;
	
	public long getUnidade_id() {
		return unidade_id;
	}

	public void setUnidade_id(long unidade_id) {
		this.unidade_id = unidade_id;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}


}
