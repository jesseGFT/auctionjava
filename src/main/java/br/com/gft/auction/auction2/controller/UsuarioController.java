//package br.com.gft.auction.auction2.controller;
//
//import java.util.List;
//
//import javax.validation.Valid;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.ModelMap;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.servlet.ModelAndView;
//
//import br.com.gft.auction.auction2.model.Usuario;
//import br.com.gft.auction.auction2.service.UsuarioService;
//
//@Controller
//public class UsuarioController {
//	
//	@Autowired
//	private UsuarioService usuarioService;
//
//	@Autowired
//	private void setService(UsuarioService usuarioService) {
//		this.usuarioService = usuarioService;
//	}
//	
//	@RequestMapping(value="/login", method = RequestMethod.GET)
//	public ModelAndView login(ModelMap model) {
//			
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		if (auth != null && auth.isAuthenticated() && !auth.getPrincipal().equals("anonymousUser")) {
//			Usuario user = usuarioService.findUserByEmail(auth.getName());
//			
//			if (user.getTipo().getRole().equals("ADMIN")) {
//				ModelAndView modelAndView =  new ModelAndView("redirect:/admin/home.htm");
//				return modelAndView;
//			}else{
//				ModelAndView modelAndView =  new ModelAndView("redirect:/default/home.htm");
//				return modelAndView;
//			}
//			
//		}else{
//			ModelAndView modelAndView =  new ModelAndView();
//			modelAndView.setViewName("login");
//			List<Usuario> userExists = usuarioService.findAll();
//			if (userExists.size() != 0){
//				for (Usuario usuario : userExists) {
//					System.out.println(usuario.getEmail());
//					System.out.println(usuario.getTipo().getRole());
//				}
//			}
//		}
//		return new ModelAndView();			
//	}
//	
//	@RequestMapping(value="/", method = RequestMethod.GET)
//	public ModelAndView index(ModelMap model) {
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.setViewName("login");
//		return new ModelAndView();			
//	}
//
//	@RequestMapping(value = "/registration", method = RequestMethod.GET)
//	public ModelAndView registration() {
//		ModelAndView modelAndView = new ModelAndView();
//		Usuario user = new Usuario();
//		modelAndView.addObject("usuario", user);
//		modelAndView.setViewName("registration");
//		return modelAndView;
//	}
//
//	@RequestMapping(value = "/registration", method = RequestMethod.POST)
//	public ModelAndView createNewUser(@Valid Usuario user, BindingResult bindingResult) {
//		ModelAndView modelAndView = new ModelAndView();
//		Usuario userExists = usuarioService.findUserByEmail(user.getEmail());
//		if (userExists != null) {
//			bindingResult.rejectValue("email", "error.user",
//					"There is already a user registered with the email provided");
//		}
//		if (bindingResult.hasErrors()) {
//			modelAndView.setViewName("registration");
//		} else {
//			usuarioService.saveUser(user);
//			modelAndView.addObject("successMessage", "User has been registered successfully");
//			modelAndView.addObject("usuario", new Usuario());
//			modelAndView.setViewName("registration");
//
//		}
//		return modelAndView;
//	}
//
//	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
//	public ModelAndView homeAdmin() {
//		ModelAndView modelAndView = new ModelAndView();
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		Usuario user = usuarioService.findUserByEmail(auth.getName());
//		modelAndView.addObject("nome",
//				"Welcome " + user.getNome() + " " + user.getSobrenome() + " (" + user.getEmail() + ")");
//		modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
//		modelAndView.setViewName("admin/home");
//		return modelAndView;
//	}
//	
//	@RequestMapping(value = "/default/home", method = RequestMethod.GET)
//	public ModelAndView homeUser() {
//		ModelAndView modelAndView = new ModelAndView();
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		Usuario user = usuarioService.findUserByEmail(auth.getName());
//		modelAndView.addObject("nome",
//				"Welcome " + user.getNome() + " " + user.getSobrenome() + " (" + user.getEmail() + ")");
//		modelAndView.addObject("defaultMessage", "Content Available Only for Users with default Role");
//		modelAndView.setViewName("default/home");
//		return modelAndView;
//	}
//
//}
