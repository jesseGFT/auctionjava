package br.com.gft.auction.auction2.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.gft.auction.auction2.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

	Produto findByCodPatrimonio(String codPatrimonio);
	
	@Query("select p from produto p where p.situacao = 1")
	public List<Produto> findAll();	
	
	@Modifying
	@Transactional 
	@Query("UPDATE produto p SET p.situacao =0 WHERE p.idProduto = :idProduto")
	int updateSituacao(@Param("idProduto") long idProduto);
	
	
	@Modifying
	@Transactional
	@Query("UPDATE produto p SET p.descricao = :descricao, p.nomeProduto = :nomeProduto,p.codPatrimonio = :codPatrimonio,p.unidade = :unidade,"
			+ "p.valorInicial = :valorInicial,p.valorPorLance = :valorPorLance WHERE p.idProduto = :idProduto")
	int updateProduto(@Param("idProduto") long idProduto,@Param("descricao") String descricao,@Param("nomeProduto") String nomeProduto,	@Param("codPatrimonio") String codPatrimonio,
			@Param("unidade") String unidade,@Param("valorInicial") double valorInicial,@Param("valorPorLance") double valorPorLance);
	 
}
